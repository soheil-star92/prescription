$(function () {
    'use strict';
    var basicAlert = $('#basic-alert');

    // Error
    if (error.length) {
        error.on('click', function () {
            Swal.fire({
                title: 'Error!',
                text: ' You clicked the button!',
                icon: 'error',
                customClass: {
                    confirmButton: 'btn btn-primary'
                },
                buttonsStyling: false
            });
        });
    }

@if(Session::has('error_at_dashboard_authentication'))
    Swal.fire({
        title: 'خطا',
        text: ''+{{Session::get('error_at_dashboard_authentication')}}+'',
        icon: 'error',
        customClass: {
        confirmButton: 'btn btn-primary'
    },
        buttonsStyling: false
    });
@endif

});
