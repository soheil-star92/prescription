@extends('layouts/fullLayoutMaster')

@section('title', 'ورود به سیستم نسخه الکترونیکی')

@section('vendor-style')
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/authentication.css')) }}">
  <link rel="stylesheet" href="{{asset(mix('css/base/plugins/extensions/ext-component-sweet-alerts.css'))}}">
@endsection

@section('content')
<div class="auth-wrapper auth-basic px-2">
  <div class="auth-inner my-2">
    <!-- Login basic -->
    <div class="card mb-0">
      <div class="card-body">
        <a href="#" class="brand-logo">
            <img src="/images/sayan/logo-1.png" alt="SayanRayan">
            <h2 class="brand-text text-primary ms-1 yekanFont"></h2>
        </a>

        <h4 class="card-title mb-1">ورود به نسخه نویسی الکترونیکی</h4>
        <p class="card-text mb-2"></p>

        <form class="auth-login-form mt-2" action="/authentication/authCheck" method="POST">
            {{csrf_field()}}
          <div class="mb-1">
            <label for="txtUserName" class="form-label">نام کاربری</label>
            <input
              type="text"
              class="form-control"
              id="iptUserName"
              name="iptUserName"
              placeholder="نام کاربری"
              aria-describedby="iptUserName"
              tabindex="1"
              autofocus
            />
          </div>

          <div class="mb-1">
            <div class="d-flex justify-content-between">
              <label class="form-label" for="txtPassword">کلمه عبور</label>
              <a href="{{url('authentication/forgot-password')}}">
                <small>فراموشی کلمه عبور</small>
              </a>
            </div>
            <div class="input-group input-group-merge form-password-toggle">
              <input
                type="password"
                class="form-control form-control-merge"
                id="iptPassword"
                name="iptPassword"
                tabindex="2"
                placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                aria-describedby="login-password"
              />
              <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
            </div>
          </div>

{{--          <div class="mb-1">--}}
{{--                <label for="txtCaptcha" class="form-label">کدامنیتی</label>--}}
{{--                <input--}}
{{--                    type="text"--}}
{{--                    class="form-control"--}}
{{--                    id="txtCaptcha"--}}
{{--                    name="txtCaptcha"--}}
{{--                    placeholder="ورود کلمه امنیتی"--}}
{{--                    aria-describedby="txtCaptcha"--}}
{{--                    tabindex="3"--}}
{{--                    autofocus--}}
{{--                />--}}
{{--          </div>--}}

          <div class="mb-1">
            <div class="form-check">
              <input class="form-check-input" name="iptRemember" type="checkbox" id="remember-me" tabindex="4" />
              <label class="form-check-label" for="remember-me">مرا به خاطر بسپار</label>
            </div>
          </div>
          <button class="btn btn-primary w-100" tabindex="4">ورود</button>
        </form>

{{--        <p class="text-center mt-2">--}}
{{--          <span>New on our platform?</span>--}}
{{--          <a href="{{url('auth/register-basic')}}">--}}
{{--            <span>Create an account</span>--}}
{{--          </a>--}}
{{--        </p>--}}

        <div class="divider my-2">
          <div class="divider-text">or</div>
        </div>

        <div class="auth-footer-btn d-flex justify-content-center">
          <a href="#" class="btn btn-facebook">
            <i data-feather="facebook"></i>
          </a>
          <a href="#" class="btn btn-twitter white">
            <i data-feather="twitter"></i>
          </a>
          <a href="#" class="btn btn-google">
            <i data-feather="mail"></i>
          </a>
          <a href="#" class="btn btn-github">
            <i data-feather="github"></i>
          </a>
        </div>
      </div>
    </div>
    <!-- /Login basic -->
  </div>
</div>
@endsection

@section('vendor-script')
<script src="{{asset(mix('vendors/js/forms/validation/jquery.validate.min.js'))}}"></script>
<script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
<script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection

@section('page-script')
<script src="{{asset(mix('js/scripts/pages/auth-login.js'))}}"></script>
<script src="{{ asset(mix('js/customize/authentication/ext-auth.js')) }}"></script>


@endsection
