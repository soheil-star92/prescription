<?php

namespace App\Http\Controllers\TestCode;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function createUser()
    {
        \Sentinel::register([
            'first_name'    => 'admin',
            'last_name'    => 'test',
            'avatar'    => 'images/avatars/noImage.png',
            'email'    => 'admin@test.com',
            'password' => '123',
        ]);
        return 1;
    }

}
